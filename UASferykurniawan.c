/*UAS FERY KURNIAWAN
SOAl NO 2*/

#include <stdio.h>

void inputData(double *absen, double *tugas, double *quiz, double *uts, double *uas) {
    printf("Masukkan nilai absen: ");
    scanf("%lf", absen);
    printf("Masukkan nilai tugas: ");
    scanf("%lf", tugas);
    printf("Masukkan nilai quiz: ");
    scanf("%lf", quiz);
    printf("Masukkan nilai uts: ");
    scanf("%lf", uts);
    printf("Masukkan nilai uas: ");
    scanf("%lf", uas);
}

double hitungNilai(double absen, double tugas, double quiz, double uts, double uas, double prosentase_absen, double prosentase_tugas, double prosentase_quiz, double prosentase_uts, double prosentase_uas) {
    return ((prosentase_absen * absen) + (prosentase_tugas * tugas) + (prosentase_quiz * quiz) + (prosentase_uts * uts) + (prosentase_uas * uas)) / 2;
}

char hitungHurufMutu(double nilai) {
    if (nilai > 85 && nilai <= 100)
        return 'A';
    else if (nilai > 70 && nilai <= 85)
        return 'B';
    else if (nilai > 55 && nilai <= 70)
        return 'C';
    else if (nilai > 40 && nilai <= 55)
        return 'D';
    else if (nilai >= 0 && nilai <= 40)
        return 'E';
}

int main() {
    double absen, tugas, quiz, uts, uas, nilai, prosentase_absen, prosentase_tugas, prosentase_quiz, prosentase_uts, prosentase_uas;
    char Huruf_Mutu;

    prosentase_absen = 0.2;
    prosentase_tugas = 0.3;
    prosentase_quiz = 0.4;
    prosentase_uts = 0.5;
    prosentase_uas = 0.6;

    inputData(&absen, &tugas, &quiz, &uts, &uas);
    nilai = hitungNilai(absen, tugas, quiz, uts, uas, prosentase_absen, prosentase_tugas, prosentase_quiz, prosentase_uts, prosentase_uas);
    Huruf_Mutu = hitungHurufMutu(nilai);
    printf("Huruf Mutu : %c\n", Huruf_Mutu);
    return 0;
}
